<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;

class UsersController extends Controller
{

	public function index(Request $request, Generator $faker) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_PORT => env('PROCESSMAKER_PORT'),
			CURLOPT_URL => env('PROCESSMAKER_URL') . "/api/1.0/workflow/users",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer " . env('PROCESSMAKER_TOKEN'),
				"cache-control: no-cache",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);

		curl_close($curl);

		return ["data" => json_decode($response)];

	}

	public function approve() {

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_PORT => env('PROCESSMAKER_PORT'),
			CURLOPT_URL => env('PROCESSMAKER_URL') . "/api/1.0/workflow/plugin-test1/pass",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer " . env('PROCESSMAKER_TOKEN'),
				"cache-control: no-cache",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);

		curl_close($curl);

		return ["data" => json_decode($response)];

	}
}
