<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/users/approve', 'UsersController@approve');
Route::get('/users/index', 'UsersController@index');
Route::post('/users/me/approve', 'UsersController@approve');

Route::get('/', function () {
    return view('welcome');
});
